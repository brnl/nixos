#
#  flake.nix *
#   ├─ ./hosts
#   │   └─ default.nix
#   ├─ ./darwin
#   │   └─ default.nix
#   └─ ./nix
#       └─ default.nix
#

{
  description = "BRNL | NixOS System Flake Configuration";

  inputs = # References Used by Flake
    {
      nixpkgs.url = "github:nixos/nixpkgs/nixos-23.11"; # Stable Nix Packages (Default)
      nixpkgs-unstable.url = "github:nixos/nixpkgs/nixos-unstable"; # Unstable Nix Packages

      # home-manager = {
      #   # User Environment Manager
      #   url = "github:nix-community/home-manager/release-23.11";
      #   inputs.nixpkgs.follows = "nixpkgs";
      # };

      # nur = {                                                               # NUR Community Packages
      #   url = "github:nix-community/NUR";                                   # Requires "nur.nixosModules.nur" to be added to the host modules
      # };

      # nixgl = {                                                             # Fixes OpenGL With Other Distros.
      #   url = "github:guibou/nixGL";
      #   inputs.nixpkgs.follows = "nixpkgs";
      # };

      # hyprland = {                                                          # Official Hyprland Flake
      #   url = "github:hyprwm/Hyprland";                                     # Requires "hyprland.nixosModules.default" to be added the host modules
      #   inputs.nixpkgs.follows = "nixpkgs-unstable";
      # };

      # plasma-manager = {                                                    # KDE Plasma User Settings Generator
      #   url = "github:pjones/plasma-manager";
      #   inputs.nixpkgs.follows = "nixpkgs";
      #   inputs.home-manager.follows = "nixpkgs";
      # };
    };

  outputs = { self, nixpkgs, nixpkgs-unstable, /*home-manager, hyprland, plasma-manager,*/ ... }: # Function telling flake which inputs to use
    let
      system = "x86_64-linux";

      pkgs = import nixpkgs {
        # Stable packages
        inherit system;
        config.allowUnfree = true; # Allow Proprietary Software
      };

      unstable = import nixpkgs-unstable {
        # Unstable packages
        inherit system;
        config.allowUnfree = true; # Allow Proprietary Software
      };

      lib = nixpkgs.lib;

      vars = {
        # Variables Used In Flake
        user = "boss";
        location = "$HOME/.setup";
        terminal = "terminator";
        editor = "vim";
      };
    in
    {
      nixosConfigurations = {
        vm = lib.nixosSystem {
          inherit system;
          specialArgs = { inherit unstable; };
          modules = [
            ./system/configuration.nix
            ./system/qemu_guest.nix
            ./pkgs/basics.nix
            ./pkgs/gaming.nix
          ];
        };
      };

      # homeConfigurations = (
      #   # Nix Configurations
      #   import ./nix {
      #     inherit (nixpkgs) lib;
      #     inherit inputs vars nixpkgs nixpkgs-unstable home-manager nixgl ;
      #   }
      # );
    };
}
