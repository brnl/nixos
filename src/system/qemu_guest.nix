{ config, pkgs, modulesPath, ... }:

{
  # Install the qemu guest agent
  services.qemuGuest.enable = true;
}
