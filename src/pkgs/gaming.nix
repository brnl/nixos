{ config, pkgs, unstable, ... }:

{

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;


  # Direct packages
  environment.systemPackages = with unstable; [
    heroic          # A Native GOG, Epic, and Amazon Games Launcher for Linux
    gzdoom          # Modder-friendly OpenGL and Vulkan source port based on the DOOM engine
    doomrunner      # Graphical launcher of ZDoom and derivatives
  ];


  # Steam
  programs.steam = {
    enable = true;
    remotePlay.openFirewall = true;
  };

}
