{ config, pkgs, unstable, ... }:

{
  environment.systemPackages = with pkgs; [

    # Multimedia
    audacity              # Sound editor with graphical UI
    spotify               # Paid audio streaming service
    mpv                   # General-purpose media player, fork of MPlayer and mplayer2
    pinta                 # Drawing/editing program modeled after Paint.NET
    drawio                # A desktop application for creating diagrams
    
    # Notes
    # unstable.obsidian     # A powerful knowledge base that works on top of a local folder of plain text Markdown files
    joplin-desktop        # An open source note taking and to-do application with synchronisation capabilities

    # Misc
    curl
    gedit                 # Basic text editor
    vim
    wget
    qbittorrent           # Featureful free software BitTorrent client
    android-tools         # Android SDK platform tools
    htop                  # An interactive process viewer
    jq                    # A lightweight and flexible command-line JSON processor
    jql                   # A JSON Query Language CLI tool built with Rust
    yq                    # Command-line YAML/XML/TOML processor - jq wrapper for YAML, XML, TOML documents
    ncdu                  # Disk usage analyzer with an ncurses interface
    meld                  # Visual diff and merge tool

    # Coding
    vscode                # Open source source code editor developed by Microsoft
    git

    # Cloud
    unstable.k9s          # Kubernetes CLI To Manage Your Clusters In Style
    kubectl               # Kubernetes CLI
    awscli2               # Unified tool to manage your AWS services
    azure-cli             # Next generation multi-platform command line experience for Azure
    kubernetes-helm       # A package manager for kubernetes

    # Hashicorp
    # packer                # A tool for creating identical machine images for multiple platforms from a single source configuration
    terraform             # Tool for building, changing, and versioning infrastructure
    terraform-docs        # A utility to generate documentation from Terraform modules in various output formats

    # Ansible
    ansible
    ansible-lint          # Best practices checker for Ansible
    molecule              # Molecule aids in the development and testing of Ansible roles
    yamllint              # A linter for YAML files

    # Python
    python3
    python311Packages.pip             # The PyPA recommended tool for installing Python packages
    # python311Packages.python-gitlab   # Interact with GitLab API
    # python311Packages.gitpython       # Python Git Library
    # python311Packages.lxml            # Pythonic binding for the libxml2 and libxslt libraries

    # Virtualisation
    virtualbox            # PC emulator
    vagrant               # A tool for building complete development environments
    podman                # A program for managing pods, containers and container images
    podman-tui            # Podman Terminal UI
    distrobox             # Wrapper around podman or docker to create and start containers

    # Package Managers
    flatpak               # Linux application sandboxing and distribution framework

    # Terminals
    kitty                 # Terminal Emulator
    terminator            # Terminal Emulator

    # Browsers
    firefox
    tor-browser           # Privacy-focused browser routing traffic through the Tor network
    ungoogled-chromium    # Web Browser

    # Password Managers
    bitwarden             # A secure and free password manager for all of your devices
    keepassxc             # Offline password manager with many features.

    # Chat
    signal-desktop        # Private, simple, and secure messenger
    telegram-desktop      # Telegram Desktop messaging app

  ];
}
