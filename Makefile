.PHONY: prepare build install

ANSIBLE_VERBOSITY ?= 1

# Check if required environment variables are set
check-env:
	@test -n "$(NIXUSER)" || (echo "NIXUSER is not set"; exit 1)
	@test -n "$(NIXADDR)" || (echo "NIXADDR is not set"; exit 1)

# Define common Ansible command options
ANSIBLE_CMD := ansible-playbook ./ansible/nixos-playbook.yml \
               -i $(NIXADDR), --user $(NIXUSER) --become --ask-become-pass \
               --ssh-extra-args "-o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no"

# Environment variables for Ansible
ANSIBLE_ENV_VARS := ANSIBLE_VERBOSITY=$(ANSIBLE_VERBOSITY) ANSIBLE_STDOUT_CALLBACK=yaml

# Prepare and test stage: Syncs the current directory to the remote server
prepare: check-env
	$(ANSIBLE_ENV_VARS) $(ANSIBLE_CMD) --tags prepare

# Build stage: Execute nixos-rebuild in switch mode on the remote server
build: check-env
	$(ANSIBLE_ENV_VARS) $(ANSIBLE_CMD) --tags build

# Install stage: Run prepare and build sequentially
install: check-env
	$(ANSIBLE_ENV_VARS) $(ANSIBLE_CMD) --tags prepare,build
