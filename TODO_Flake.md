# Automation
<!-- ansible -->
<!-- packer -->
<!-- aws-cli -->
<!-- azure-cli-bin -->
<!-- helm -->
<!-- kubectl -->
<!-- k9s -->
<!-- terraform -->
<!-- terraform-docs -->

# Misc
<!-- android-tools -->
pixelflasher
<!-- qbittorrent -->
<!-- torbrowser -->


# Gaming
<!-- gzdoom -->

# Coding
<!-- visual-studio-code -->

# Virtualisation
<!-- virtualbox -->
<!-- vagrant -->
docker
<!-- podman -->
virt-viewer
libvirt
<!-- distrobox -->
<!-- flatpak -->

# Python
<!-- python-gitlab -->
<!-- python-gitpython -->
<!-- python-lxml -->
<!-- python-pip -->
<!-- python-zabbix-api -->

# Backup
deja-dup
restic
timeshift

# Audio / Video
cava
<!-- audacity -->
<!-- vlc -->
<!-- mpv -->
<!-- spotify -->

# Passwords
<!-- bitwarden -->
<!-- keepassxc -->
yubikey-manager-qt
yubikey-personalization-gui
yubioath-desktop

# Tools
<!-- htop -->
<!-- jq -->
<!-- yq -->
<!-- ncdu -->
<!-- meld -->

# Notes
<!-- Obsidian -->
<!-- Joplin -->

# Images
<!-- pinta -->
<!-- drawio -->

# Chat
<!-- signal-desktop -->
<!-- telegram-desktop -->
